from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
import accueil
from django.template import loader
from accueil import models
from accueil.models import Article,Project,User
from django import forms
from accueil.forms import SearchForm
from accueil.traitement import contient
from accueil.forms import ChoixProjet,CreationForm
# Create your views here.

def fonction_accueil(request):
    template= loader.get_template("./index-1.html")
    return HttpResponse(template.render(request=request))

# def search_query(request):
#     query=request.GET.['query']


# Create your views here.
def affichage_accueil(request):
     return render (request, 'index.html')
     #message="salut"
     #return HttpResponse(message)
     #template = loader.get_template('./index.html')
     #return HttpResponse(template.render(request=request)

def affichage_cas(request):
     return render (request, 'cas.html')

def affichage_tuto(request):
    res = Article.objects.all()
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            res = []
            for titre in Article.objects.all():
                if contient(form.cleaned_data['recherche'],titre.name_article):
                    res.append(titre)
                    print(titre.name_article)
            form = SearchForm()
            return render(request,'products.html',{'form':form,'liste_article':res})
    else:
        form = SearchForm()

    return render(request, 'products.html', {'form':form,'liste_article':res})

# Create your views here.

# def first_connection(request):
#     return HttpResponse("Bienvenue")

def first_connection(request):
    if request.method == "POST":
        form = ChoixProjet(request.POST)
        if form.is_valid():
            objet_projet = accueil.models.Project.objects.get(name_project = form.cleaned_data['liste'])
            new_user = User(name_user = '', projet = objet_projet)
            new_user.save()
            return(HttpResponseRedirect('/accueil/page_principale'))
    else:
        form = ChoixProjet()
        return render(request, "liste_déroul_prem_page.html", {'form':form})

def creation_tuto(request):
    res = Article.objects.all()
    if request.method == 'POST':
        form = CreationForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accueil/tuto/')
    else:
        form = CreationForm()

    return render(request, 'creation_tuto.html', {'form':form})