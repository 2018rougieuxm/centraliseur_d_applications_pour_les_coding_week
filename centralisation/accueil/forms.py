from django import forms
import accueil
from accueil.models import Project,Article
from django.forms import ModelForm

class SearchForm(forms.Form):
    recherche = forms.CharField(label = '' , max_length=100)

class ChoixProjet(forms.Form):
    liste = forms.ChoiceField(choices=[(projet.name_project,projet.name_project) for projet in accueil.models.Project.objects.all()])

class CreationForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['name_article','contenu']