from django.db import models
from django.utils.deconstruct import deconstructible

class Project(models.Model):
    name_project = models.CharField(max_length = 200, unique = True)
    numero = models.IntegerField()

@deconstructible
class User(models.Model):
    name_user = models.CharField(max_length=200)
    projet = models.ForeignKey(Project, on_delete=models.CASCADE, blank = True)
    
class Article(models.Model):
    name_article = models.CharField(max_length=200, unique = True)
    contenu = models.FileField(upload_to='accueil/static/article/')
    author = models.ForeignKey(User, on_delete=models.CASCADE,default=User.objects.get(pk=1))
    
    