from django.contrib import admin
from django.conf.urls import *
from django.urls import path,include
from django.conf import settings
import accueil
from accueil import views

urlpatterns = [
    url('connexion/', accueil.views.fonction_accueil, name="connexion"),
    path('cas/',views.affichage_cas),
    url(r'^$', accueil.views.first_connection, name = 'Page de la première connexion'),
    path('creation_tuto/',accueil.views.creation_tuto),
    path('page_principale/',views.affichage_accueil),
    path('tuto/',views.affichage_tuto),
    path('cas/',views.affichage_cas),
]
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
] + urlpatterns

